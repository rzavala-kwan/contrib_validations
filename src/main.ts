import { ModuleFileValidationRule } from "./contrib/validations/module-file.validation.rule";
import { CommonCategoryValidationRule } from "./contrib/validations/common-category.validation.rule";
import { GoRefFileValidationRule } from "./contrib/validations/go-ref.validation.rule";
import { UploadInfoHelper, SchemaCollector } from "./contrib/validations/utils/upload-info.helper";
import { HasCategoryValidationRule } from "./contrib/validations/has-category.validation.rule";
import { HasJSONFileValidationRule } from "./contrib/validations/has-json-file.validation.rule";
import { HasNameAndTypeValidationRule } from "./contrib/validations/name-and-type.validation.rule";
import { ValidationError } from "./models/validation";

const fs = require('fs');
// const path = "./tests/many_ts"; // Aquí vamos a meter los archivos que serán evaluados
// const path = "./tests/twilio"; // Aquí vamos a meter los archivos que serán evaluados
// const path = "./tests/wiactivity"; // Aquí vamos a meter los archivos que serán evaluados
const path = "./tests/flogotest"; // Aquí vamos a meter los archivos que serán evaluados
const files = fs.readdirSync(path);

class Startup {
  public static main(): number {
    let files = UploadInfoHelper.FilesUploaded(path);
    let schemaCollector = new SchemaCollector();

    let results: ValidationError[] = [];
    let tmpRes: ValidationError[];
    let evaluation0 = new HasJSONFileValidationRule(files);
    results.push(...evaluation0.responseCollection);

    if (evaluation0.responseCollection.length === 0) {
      results.push(
        // ... new HasNameAndTypeValidationRule(files, schemaCollector).responseCollection,
        // ... new HasCategoryValidationRule(files, schemaCollector).responseCollection,
        // ... new ModuleFileValidationRule(files).responseCollection,
        // ... new CommonCategoryValidationRule(files, schemaCollector).responseCollection,
        ... new GoRefFileValidationRule(files, schemaCollector).responseCollection,
        // ... new HasNameAndTypeValidationRule(files, schemaCollector).responseCollection
      );
    }

    console.log("Results > ", results);
    return 0;
  }
}

console.log("Started > ", Startup.main());