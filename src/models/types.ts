export type STRING_MAP<P> = Map<string, P>;
export type ENUM_MAP<E, P> = Map<E, P>;

export function strEnum<T extends string>(o: Array<T>): {[K in T]: K} {
  return o.reduce((res, key) => {
    res[key] = key;
    return res;
  }, Object.create(null));
}

// http://2ality.com/2015/08/es6-map-json.html
export function mapToJson(map): string {
  return JSON.stringify([...map]);
}
export function jsonToMap<K, V>(jsonStr): Map<K, V> {
  return new Map<K, V>(JSON.parse(jsonStr));
}

export type ARRAY_STR_ARRAY = [string[]];
/**
 * JSON Schema Type enumerations
 * https://developers.google.com/discovery/v1/type-format
 * http://json-schema.org/latest/json-schema-validation.html#rfc.section.3.2
 * @type {{[K in string | string | string | string | string | string | string]:K}}
 */
export const JSON_TYPE = strEnum(["any", "array", "boolean", "integer", "number", "object", "string"]);
export type JSON_TYPE = keyof typeof JSON_TYPE;

/**
 * JSON Schema Type Format enumerations
 * https://developers.google.com/discovery/v1/type-format
 * http://json-schema.org/latest/json-schema-validation.html#rfc.section.3.2
 * @type {{[K in string | string | string | string | string | string | string | string | string | string | string | string | string | string]:K}}
 */
export const JSON_FORMAT = strEnum([
  "int32",
  "uint32",
  "double",
  "float",
  "byte",
  "date",
  "date_time",
  "int64",
  "uint64",
  "object",
  "email",
  "hostname",
  "ipv4",
  "ipv6",
  "uri",
  "uri_reference",
  "uri_template",
  "json_pointer"]);
export type JSON_FORMAT = keyof typeof JSON_FORMAT;

/**
 * GO Lang Type enumerations
 * @type {{[K in string | string | string | string | string | string | string | string]:K}}
 */
export const GOLANG_TYPE = strEnum(["string", "integer", "number", "boolean", "object", "array", "any", "complex_object"]);
export type GOLANG_TYPE = keyof typeof GOLANG_TYPE;

/**
 * JSON Schema Type Information
 */
export interface JSONSCHEMATypeInfo {
  type: JSON_TYPE;
  format?: JSON_FORMAT;
  pattern?: string;
  min?: number;
  max?: number;
}
export type JSON_SCHEMA_TYPE = JSONSCHEMATypeInfo;

const JSON_RT_TYPE_MAPPING: ENUM_MAP<GOLANG_TYPE, JSON_SCHEMA_TYPE> = new Map<GOLANG_TYPE, JSON_SCHEMA_TYPE>();
JSON_RT_TYPE_MAPPING[GOLANG_TYPE.string] = { type: JSON_TYPE.string };
JSON_RT_TYPE_MAPPING[GOLANG_TYPE.integer] = { type: JSON_TYPE.integer };
JSON_RT_TYPE_MAPPING[GOLANG_TYPE.number] = { type: JSON_TYPE.number };
JSON_RT_TYPE_MAPPING[GOLANG_TYPE.boolean] = { type: JSON_TYPE.boolean };
JSON_RT_TYPE_MAPPING[GOLANG_TYPE.object] = { type: JSON_TYPE.object };
JSON_RT_TYPE_MAPPING[GOLANG_TYPE.array] = { type: JSON_TYPE.array };
// JSON_RT_TYPE_MAPPING[GOLANG_TYPE.any] = { type: JSON_TYPE.string };
JSON_RT_TYPE_MAPPING[GOLANG_TYPE.complex_object] = { type: JSON_TYPE.object };

/**
 * Returns a {{JSON_SCHEMA_TYPE}}
 * @param {GOLANG_TYPE} rtType
 * @return {JSON_SCHEMA_TYPE}
 * @constructor
 */
export function RTtoJSONTypeMap(rtType: GOLANG_TYPE): JSON_SCHEMA_TYPE {
  return JSON_RT_TYPE_MAPPING[rtType];
}




