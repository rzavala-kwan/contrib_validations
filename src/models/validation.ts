import { STRING_MAP } from "./types";

/**
 * Validation Error Interface
 */
export interface IValidationError {
  /**
   * Error Code
   * @private
   */
  errorCode: string;
  /**
   * Error Message
   * @private
   */
  errorMsg: string;
  /**
   * Error Context
   * @private
   */
  context?: STRING_MAP<string>;

  /**
   * Returns Error Code
   * @return {string}
   */
  getErrorCode(): string;

  /**
   * Returns Error Message
   * @return {string}
   */
  getErrorMessage(): string;

  /**
   * Returns a string representation of the Error
   * @return {string}
   */
  toString(): string;

  /**
   * Set Context Map for the Error i.e Flow -> Activity -> Property
   * @private Internal Use
   * @param {STRING_MAP<string>} context
   */
  setContext(context: STRING_MAP<string>);
}

/**
 * Validation Error Implementation
 */
export class ValidationError implements IValidationError {
  /**
   * Static Constructor Create a new validation error instance
   * @param {string} errorCode
   * @param {string} errorMsg
   * @return {IValidationError}
   */
  public static newError(errorCode: string, errorMsg: string): IValidationError {
    return new ValidationError(errorCode, errorMsg);
  }

  /**
   * Constructor - Create a new validation error instance
   * @constructor
   * @param {string} errorCode
   * @param {string} errorMsg
   * @param {STRING_MAP<string>} context
   */
  constructor(public errorCode: string, public errorMsg: string, public context?: STRING_MAP<string>) {
    if (!context) {
      context = new Map<string, string>();
    }
  }

  /**
   * Returns Error Code
   * @return {string}
   */
  public getErrorCode(): string {
    return this.errorCode;
  }

  /**
   * Returns Error Message
   * @return {string}
   */
  public getErrorMessage(): string {
    return this.errorMsg;
  }

  /**
   * Returns a string representation of the error
   * @return {string}
   */
  public toString(): string {
    return this.errorCode + ":" + this.errorMsg;
  }

  /**
   * Set the error context
   * @private Internal Use only
   * @param {STRING_MAP<string>} context
   */
  public setContext(context: STRING_MAP<string>) {
    this.context = context;
  }
}

/**
 * Field Validation State Enumeration
 */
export enum EnumValidationState {
  /**
   * The field is read only based on the bit flag
   * @type {number}
   */
  WI_READONLY = 0x2,
  /**
   *  The field is visible based on the bit flag
   * @type {number}
   */
  WI_VISIBLE = 0x4,
  /**
   * The field is valid based on the bit flag
   * @type {number}
   */
  WI_VALID = 0x8
}

/**
 * Validation Result interface
 */
export interface IValidationResult {
  /**
   * Validation State
   * @private
   */
  state: EnumValidationState | number;
  /**
   * Validation Errors
   * @private
   */
  errors: IValidationError[];

  /**
   * Returns the Validation Read Only state
   * @return {boolean}
   */
  isReadOnly(): boolean;

  /**
   * Returns the Validation Visible state
   * @return {boolean}
   */
  isVisible(): boolean;

  /**
   * Returns the Validation validity state
   * @return {boolean}
   */
  isValid(): boolean;

  /**
   * Sets the Validation Read Only state
   * @param {boolean} res
   * @return {IValidationResult}
   */
  setReadOnly(res: boolean): IValidationResult;

  /**
   * Sets the Validation Visible state
   * @param {boolean} res
   * @return {IValidationResult}
   */
  setVisible(res: boolean): IValidationResult;

  /**
   * Sets the Validation valid state
   * @param {boolean} res
   * @return {IValidationResult}
   */
  setValid(res: boolean): IValidationResult;

  /**
   * Adds Validation error
   * @param {string} errorCode
   * @param {string} errorMessage
   * @return {IValidationResult}
   */
  setError(errorCode: string, errorMessage: string): IValidationResult;

  /**
   * Returns Validation Errors
   * @return {IValidationError[]}
   */
  getErrors(): IValidationError[];
}

/**
 * Validation Result Implementation
 */
export class ValidationResult implements IValidationResult {

  // tslint:disable-next-line:no-bitwise
  state: EnumValidationState | number = EnumValidationState.WI_VALID | EnumValidationState.WI_VISIBLE;
  errors: IValidationError[] = [];

  /**
   * Static constructor
   * @param {IValidationError | IValidationError[]} errors
   * @return {IValidationResult}
   */
  static newValidationResult(errors?: IValidationError | IValidationError[]): IValidationResult {
    return new ValidationResult(errors);
  }

  /**
   * @constructor
   * @param {IValidationError | IValidationError[]} errors
   */
  constructor(errors?: IValidationError | IValidationError[]) {
    this.errors = [];
    if (errors) {
      this.setValid(false);
      if (!Array.isArray(errors)) {
        this.errors.push(errors);
      } else {
        this.errors = this.errors.concat(errors);
      }
    }
  }

  // tslint:disable:no-bitwise

  isReadOnly(): boolean {
    return ((this.state & EnumValidationState.WI_READONLY) === EnumValidationState.WI_READONLY);
  }

  isVisible(): boolean {
    return ((this.state & EnumValidationState.WI_VISIBLE) === EnumValidationState.WI_VISIBLE);
  }

  isValid(): boolean {
    return ((this.state & EnumValidationState.WI_VALID) === EnumValidationState.WI_VALID);
  }

  setReadOnly(res: boolean): IValidationResult {
    this.state = res ? this.state | EnumValidationState.WI_READONLY : this.state & ~EnumValidationState.WI_READONLY;
    return this;
  }

  setVisible(res: boolean): IValidationResult {
    this.state &= res ? this.state | EnumValidationState.WI_VISIBLE : this.state & ~EnumValidationState.WI_VISIBLE;
    return this;
  }

  setValid(res: boolean): IValidationResult {
    this.state &= res ? this.state | EnumValidationState.WI_VALID : this.state & ~EnumValidationState.WI_VALID;
    return this;
  }

  setError(errorCode: string, errorMessage: string): IValidationResult {
    this.errors.push(ValidationError.newError(errorCode, errorMessage));
    return this;
  }

  getErrors(): IValidationError[] {
    return this.errors;
  }
}
