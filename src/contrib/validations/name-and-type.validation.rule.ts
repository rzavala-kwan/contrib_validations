import * as fs from "fs";
import * as fspath from "path";
import { ValidationError } from "../../models/validation";
import { UploadedFolder, UploadInfoHelper, SchemaCollector } from "./utils/upload-info.helper";

export class HasNameAndTypeValidationRule {
  responseCollection: ValidationError[] = [];

  constructor(uploadedDir: UploadedFolder[], schemaCollector: SchemaCollector) {
    uploadedDir.forEach(folder => {

      if (folder.files.length > 0) {
        let response: ValidationError;
        let jsonFiles = UploadInfoHelper.ActivityFilesInArray(folder.files);

        jsonFiles.forEach(jsonFile => {
          let jsonFilePath = fspath.join(folder.path, jsonFile);
          let jsonSchema = schemaCollector.get(jsonFilePath);
          let activityInfo = UploadInfoHelper.SchemaInfo(jsonSchema);
          if (!activityInfo.error) {

            if (!activityInfo.name) {
              response = new ValidationError("error", "No NAME field found.");
              if (response) {
                let activityName = UploadInfoHelper.JsonFileName(jsonFile);
                UploadInfoHelper.AddActivityId(uploadedDir, response, fspath.basename(folder.path), activityName);
                this.responseCollection.push(response);
              }
            }

            if (!activityInfo.type) {
              response = new ValidationError("error", "No TYPE field found.");
              if (response) {
                let activityName = UploadInfoHelper.JsonFileName(jsonFile);
                UploadInfoHelper.AddActivityId(uploadedDir, response, fspath.basename(folder.path), activityName);
                this.responseCollection.push(response);
              }
            }
          } else {
            response = new ValidationError("error", activityInfo.error);
            this.responseCollection.push(response);
          }
        })

      }

    });
  }
}
