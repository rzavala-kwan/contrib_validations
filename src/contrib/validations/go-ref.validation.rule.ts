// tslint:disable:forin
import * as fs from "fs";
import * as fspath from "path";
import { ValidationError } from "../../models/validation";
import { UploadedFolder, UploadInfoHelper, SchemaCollector } from "./utils/upload-info.helper";

export class GoRefFileValidationRule {
  responseCollection: ValidationError[] = [];

  constructor(uploadedDir: UploadedFolder[], schemaCollector: SchemaCollector) {
    uploadedDir.forEach(folder => {
      let response: ValidationError;
      let goFilePresent = folder.files.filter(name => name.endsWith(".go")).length > 0;
      let errorExists = true;
      let errorCollection: ValidationError[] = [];
      UploadInfoHelper.ActivityFilesInArray(folder.files)
        .forEach(jsonFile => {
          if (errorExists) {
            let responses: ValidationError[] = [];
            let jsonFilePath = fspath.join(folder.path, jsonFile);
            let jsonSchema = schemaCollector.get(jsonFilePath);
            let jsonName = UploadInfoHelper.JsonFileName(jsonFile);
            let activityInfo = UploadInfoHelper.SchemaInfo(jsonSchema, folder.files);

            if (!activityInfo.error) {
              let activityType = UploadInfoHelper.JsonFileName(jsonFile);
              let ref = activityInfo.schema.ref;

              if (!goFilePresent) { // No .go files
                if (activityInfo.isFlogo) { // Flogo activity has to have .go
                  responses.push(new ValidationError("error", `No ".go" file present for this Flogo activity.`));
                } else {
                  if (jsonName !== "connector") { // Connectors have .go as optional
                    responses.push(new ValidationError("error", `No ".go" file present for this ${jsonName}.`));
                  }
                }
              } else { // There're .go files. Let's evaluate "ref"
                if (ref) {
                  let refPathExpected = fspath.join(activityType, folder.relativePath);
                  if (!ref.endsWith(refPathExpected)) {
                    responses.push(new ValidationError("error", `"ref" attribute doesn't point to ${jsonName} directory.`));
                  } else if (!activityInfo.isFlogo) { // If not flogo, evaluate "category" in ref
                    refPathExpected = fspath.join(activityInfo.category, refPathExpected);
                    if (!ref.endsWith(refPathExpected)) {
                      responses.push(new ValidationError("error", `"ref" attribute doesn't include ${jsonName} category.`));
                    } else { // Victory! At least one schema is OK
                      errorExists = false;
                    }
                  } else {  // Victory! This flogo activity is OK
                    errorExists = false;
                  }
                } else {
                  responses.push(new ValidationError("error", `There're ".go" files present, but no "ref" attribute is in ${jsonName} schema.`));
                }
              }

              if (responses.length > 0) {
                for (let i in responses) {
                  UploadInfoHelper.AddActivityId(uploadedDir, responses[i], fspath.basename(folder.path), activityType);
                }
                errorCollection.push(...responses);
              }
            } else {
              responses.push(new ValidationError("error", activityInfo.error));
            }
          }
        });

      if (errorExists) {
        this.responseCollection = errorCollection;
      }
    });
  }
}
