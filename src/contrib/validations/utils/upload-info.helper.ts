import * as fs from "fs";
import * as fspath from "path";
import { ValidationError } from "../../../models/validation";

export interface UploadedFolder {
  path: string;
  relativePath: string;
  files: string[];
}

export class UploadInfoHelper {
  /**
   * Provides an array of files contained in folders
   * @param path Folder in where the zip files are stored
   */
  static FilesUploaded(path: string) {
    let folders: UploadedFolder[] = [];
    let files = fs.readdirSync(path);
    let directories = files.filter(file => fs.lstatSync(fspath.join(path, file)).isDirectory());
    if (directories.length > 0) {
      directories.forEach(directory => {
        let dirPath = fspath.join(path, directory);
        if (UploadInfoHelper.DirectoryHasActivity(dirPath)) {
          folders.push({
            path: dirPath,
            relativePath: fspath.join(fspath.basename(path), directory),
            files: fs.readdirSync(dirPath)
          });
        }
      });
    } else {
      if (files.filter(file => file.endsWith(".json")) && UploadInfoHelper.DirectoryHasActivity(path)) {
        folders.push({
          path: path,
          relativePath: fspath.basename(path),
          files: files
        });
      }
    }
    return folders;
  }

  /**
 * Returns an object with information of an schema
 */
  static SchemaInfo(schema: any, files?: string[]) {
    let r = {
      category: "",
      isFlogo: true,
      name: null,
      error: null,
      type: null,
      schema: null
    };

    if (typeof schema === "string") {
      try {
        r.schema = JSON.parse(schema);
      } catch (e) {
        r.error = "JSON schema could not be validated.";
      }
    }

    if (!r.error) {
      r.name = r.schema.name;
      r.type = r.schema.type;
      if (r.schema.display && r.schema.display.category) {
        r.category = r.schema.display.category.toString();
        r.isFlogo = false;
      }

      // IPAS-5969 > #23 & #24. Flogo activity or wi activity will be determined by presence of .ts file.
      // No .ts files && No .display.category > Is FLogo
      if (files && files.length > 0 && files.filter(file => file.endsWith(".ts")).length > 0) {
        r.isFlogo = false;
      }
    }
    return r;
  }

  // Add activity name to response as id if needed.
  static AddActivityId(uploadedDir: UploadedFolder[], response: ValidationError, dirName: string, activityName?: string) {
    let differ = false;
    let activityNameSet = false;
    let labels: string[] = [];
    if (uploadedDir.length > 1) { // Many dirs
      labels.push(dirName);
      differ = true;
      if (activityName) {
        labels.push(activityName);
        activityNameSet = true;
      }
    }

    if (!activityNameSet) {
      let otherActivities = uploadedDir
        .filter(dir => dir.path.endsWith(dirName))
        .map(dir => dir.files)[0];
      if (otherActivities.length > 0) { // Many activities in same folder
        labels.push(activityName);
        differ = true;
      }
    }

    if (differ) {
      response.errorMsg += ` [${labels.join(".")}]`;
    }
  }

  static ActivityNames() {
    return ["trigger", "activity", "connector"];
  }

  static ActivityFilesInArray(files: string[]) {
    return files.filter(file => file.endsWith(".json") && UploadInfoHelper.ActivityNames().indexOf(UploadInfoHelper.JsonFileName(file)) > -1);
  }

  static DirectoryHasActivity(path: string) {
    return UploadInfoHelper.ActivityFilesInArray(fs.readdirSync(path));
  }

  static JsonFileName(file: string) {
    return fspath.basename(file, ".json");
  }
}

export class SchemaCollector {
  collection = {};

  get(jsonFilePath: string) {
    if (!this.collection[jsonFilePath]) {
      this.collection[jsonFilePath] = fs.readFileSync(jsonFilePath, "utf8");
    }
    return this.collection[jsonFilePath];
  }
}
