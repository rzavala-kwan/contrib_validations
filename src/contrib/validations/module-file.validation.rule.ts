import * as fs from "fs";
import * as fspath from "path";
import * as ts from "typescript";
import { ValidationError } from "../../models/validation";
import { UploadedFolder, UploadInfoHelper, SchemaCollector } from "./utils/upload-info.helper";

export class ModuleFileValidationRule {
  responseCollection: ValidationError[] = [];

  constructor(uploadedDir: UploadedFolder[]) {
    uploadedDir.forEach(folder => {
      let response: ValidationError;
      let tsFiles = folder.files.filter(name => name.endsWith(".ts"));
      let moduleFiles = folder.files.filter(name => name.endsWith(".module.ts"));
      if (tsFiles.length > 0) {
        if (moduleFiles.length === 0) {
          response = new ValidationError("error", "No \".module.ts\" found in \".ts\" files provided.");
        } else if (moduleFiles.length > 1) {
          response = new ValidationError("error", "Only one \".module.ts\" file is allowed per activity.");
        } else {
          let modulePath = fspath.join(folder.path, moduleFiles[0]);
          let sourceFile = ts.createSourceFile(moduleFiles[0], fs.readFileSync(modulePath).toString(), ts.ScriptTarget.Latest, true);
          let sourceFileProps = delint(sourceFile);
          if (!sourceFileProps.hasInjectable && !sourceFileProps.hasNgModule) {
            response = new ValidationError("error", "Invalid \".module.ts\" file. No Injectable or NgModule found.");
          } 
        }

        if (response) {
          UploadInfoHelper.AddActivityId(uploadedDir, response, fspath.basename(folder.path));
          this.responseCollection.push(response);
        }
      }
    });
  }
}

export function delint(sourceFile: ts.SourceFile) {
  let hasInjectable = false;
  let hasNgModule = false;

  delintNode(sourceFile);

  function delintNode(node: ts.Node) {
    if (node.flags === 8192 && (<any>node).escapedText === "Injectable") {
      hasInjectable = true;
    }
    if (node.flags === 8192 && (<any>node).escapedText === "NgModule") {
      hasNgModule = true;
    }
    ts.forEachChild(node, delintNode);
  }
  
  return {hasInjectable, hasNgModule};
}
