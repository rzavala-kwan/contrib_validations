import * as fs from "fs";
import * as fspath from "path";
import { ValidationError } from "../../models/validation";
import { UploadedFolder, UploadInfoHelper, SchemaCollector } from "./utils/upload-info.helper";

export class CommonTypeValidationRule {
  responseCollection: ValidationError[] = [];
  isFlogo: boolean;

  constructor(uploadedDir: UploadedFolder[]) {
    let response: ValidationError;
    let uploadedType = "";
    uploadedDir.forEach(folder => {
      UploadInfoHelper.ActivityFilesInArray(folder.files).forEach(jsonFile => {
        let jsonFilePath = fspath.join(folder.path, jsonFile);
        let jsonSchema = fs.readFileSync(jsonFilePath, "utf8");
        let activityInfo = UploadInfoHelper.SchemaInfo(jsonSchema, folder.files);
        if (!activityInfo.error) {

          if (typeof this.isFlogo === "undefined") {
            this.isFlogo = activityInfo.isFlogo;
          } else if (this.isFlogo !== activityInfo.isFlogo) {
            response = new ValidationError("error", "All uploaded activities should share same TYPE.");
          }

        } else {
          response = new ValidationError("error", activityInfo.error);
        }
        if (response) {
          return;
        }
      });
      if (response) {
        return;
      }
    });
    if (response) {
      this.responseCollection.push(response);
    }
  }
}
