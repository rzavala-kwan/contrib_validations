import * as fs from "fs";
import * as fspath from "path";
import { ValidationError } from "../../models/validation";
import { UploadedFolder, UploadInfoHelper, SchemaCollector } from "./utils/upload-info.helper";

export class HasJSONFileValidationRule {
  responseCollection: ValidationError[] = [];

  constructor(uploadedDir: UploadedFolder[]) {
    let jsonFound = false;
    let response: ValidationError;
    uploadedDir.forEach(folder => {
      if (folder.files.length > 0) {
        let jsonFiles = UploadInfoHelper.ActivityFilesInArray(folder.files);
        if (jsonFiles.length > 0) {
          jsonFiles.forEach(jsonF => {
            let jsonFilePath = fspath.join(folder.path, jsonF);
            if (UploadInfoHelper.ActivityFilesInArray(folder.files).length === 0) {
              response = new ValidationError("error", "No JSON file found.");
            } else {
              let jsonSchema = fs.readFileSync(jsonFilePath, "utf8");
              let activityInfo = UploadInfoHelper.SchemaInfo(jsonSchema, folder.files);
              if (!activityInfo.error) {
                jsonFound = true;
              } else {
                response = new ValidationError("error", activityInfo.error);
                this.responseCollection.push(response);
                return;
              }
            }


          })
        } else {
          response = new ValidationError("error", "No JSON file found.");
        }


      } else {
        response = new ValidationError("error", "No files found.");
      }
      if (jsonFound) {
        return;
      }
    });
    if (!jsonFound && response) {
      this.responseCollection.push(response);
    }
  }
}
