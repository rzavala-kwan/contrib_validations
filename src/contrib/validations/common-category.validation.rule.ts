import * as fs from "fs";
import * as fspath from "path";
import { ValidationError } from "../../models/validation";
import { UploadedFolder, UploadInfoHelper, SchemaCollector } from "./utils/upload-info.helper";

export class CommonCategoryValidationRule {
  responseCollection: ValidationError[] = [];

  constructor(uploadedDir: UploadedFolder[], schemaCollector: SchemaCollector) {
    let uploadedCategory = "";
    uploadedDir.forEach(folder => {
      UploadInfoHelper.ActivityFilesInArray(folder.files).forEach(jsonFile => {
        let response: ValidationError;
        let jsonFilePath = fspath.join(folder.path, jsonFile);
        let jsonSchema = schemaCollector.get(jsonFilePath);
        let activityInfo = UploadInfoHelper.SchemaInfo(jsonSchema, folder.files);
        if (!activityInfo.error) {
          if (uploadedCategory === "") {
            uploadedCategory = activityInfo.category;
          } else if (uploadedCategory !== activityInfo.category) {
            response = new ValidationError("error", "All uploaded activities should share same category.");
          }
        } else {
          response = new ValidationError("error", activityInfo.error);
        }
        if (response) {
          let activityName = UploadInfoHelper.JsonFileName(jsonFile); // Get activity name
          UploadInfoHelper.AddActivityId(uploadedDir, response, fspath.basename(folder.path), activityName);
          this.responseCollection.push(response);
        }
      });
    });
  }
}
